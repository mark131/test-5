const router = require('express').Router()
const Entry = require('../models/Entry')
const {verifyUser,verifyAdmin}= require('../utils')


//create Entry
router.post('/',verifyUser,(req,res,next) =>{
    Entry.create({
        entryName:req.body.entryName,
        userId:req.user.id,
        categoryId:req.body.categoryId,
        amount:req.body.amount
    })
    .then( Entry => {
        res.send(Entry)
    })
    .catch(next)
})

//get Entry
router.get('/',verifyUser, (req,res,next) =>{
    Entry.find({userId: req.user.id})
    .then( categories => {
        res.send(categories)
    }).catch(next)
})
//update Entry
router.put('/:id',verifyUser, (req,res,next) =>{
    Entry.findByIdAndUpdate(req.params.id, req.body,{new: true})
    .then(entries => {
        res.send(entries)
    }).catch(next)
})
//delete specific Entry
router.delete('/:id', verifyUser,(req,res,next)=>{
    Entry.findByIdAndDelete(req.params.id)
    .then( entries =>{
        res.send(entries)
    })
    .catch(next)
})
module.exports = router