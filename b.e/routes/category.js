const router = require('express').Router()
const category = require('../models/category')
const {verifyUser,verifyAdmin}= require('../utils')


//create category
router.post('/', verifyUser, (req,res,next) =>{
    category.create({
        userId:req.user.id,
        name:req.body.name
    })
    .then( product => {
        res.send(product)
    })
    .catch(next)
})

//get specific user's category
router.get('/',verifyUser, (req,res,next) =>{
    category.find({userId: req.user.id})
    .then( categories => {
        res.send(categories)
    }).catch(next)
})

router.get('/all',(req,res,next)=>{
    category.find()
    .then(categories =>{
        res.send(categories)
    }).catch(next)
})

//update specific user's category
router.put('/:id',verifyUser, (req,res,next) =>{
    category.findByIdAndUpdate(req.params.id, req.body,{new: true})
    .then(categories => {
        res.send(categories)
    }).catch(next)
})

//delete specific user's category
router.delete('/:id', verifyUser,(req,res,next)=>{
    category.findByIdAndDelete(req.params.id)
    .then( categories =>{
        res.send(categories)
    })
    .catch(next)
})
module.exports = router
