const mongoose = require('mongoose')

const EntrySchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true    
    },
    categoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category',
        required: true
      },
    entryName: {
      type: String,
      required: true
    },
    amount: {
        type: Number,
        required: true
      }
})

module.exports = mongoose.model('Entry', EntrySchema)