const mongoose = require('mongoose')

const CategorySchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Entry',
        required: true
    },
    name: {
        type: String,
        required: true
    }

})

module.exports = mongoose.model('category', CategorySchema)