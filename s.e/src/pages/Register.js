import React from 'react'
import RegisterForm from '../components/RegisterForm'
import { Container, Row, Col } from 'react-bootstrap'
import { useState } from 'react'
import { Redirect } from 'react-router-dom'

function Register() {
    const [isRedirect,setIsRedirect] = useState(false)
    return (
        isRedirect ?
        <Redirect to="/login"/> :
        <Container>
        <Row>
            <Col className="mx-auto" xs={12} sm={10} md={6}> 
                <RegisterForm setIsRedirect={setIsRedirect} />
            </Col>
        </Row>
    </Container>
    )
}

export default Register
