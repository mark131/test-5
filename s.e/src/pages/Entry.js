import React, {useState, useEffect, useContext} from 'react'


import { Modal } from 'react-bootstrap'


import AddEntry from '../components/AddEntry'
import EditEntry from '../components/EditEntry'
import ListEntry from '../components/ListEntry'
import { ApplicationContext } from './../contexts/ApplicationContext';

const EditEntryModal = ({visible, entries, setIsEditing, editFields, setEntries}) => {
  
    const handleClose = (data) => {
        const formattedEntries = entries.filter(entry => entry._id !== editFields._id)

        setIsEditing(false)
        setEntries([data, ...formattedEntries,])
    }
  
    return (  
        <Modal show={visible} onHide={handleClose} animation={false}>
          <Modal.Header closeButton>
            <Modal.Title>Edit Entry</Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <EditEntry onClose={(data) => handleClose(data)} editFields={editFields} setEntries={setEntries} />
          </Modal.Body>
        </Modal>
    );
  }

const Entry = () => {
    const [isEditing, setIsEditing] = useState(false)
    const [editFields, setEntryEditFields] = useState(null)
    const [entries, setEntries] = useState([])
    const  {categories} = useContext(ApplicationContext)

    useEffect(()=>{
        if (!setIsEditing) {
            let access = localStorage.getItem('token');
        fetch('http://localhost:8000/api/Entry',{
          headers:{
              Authorization: `Bearer ${access}`
          }  
        })
        .then(res => res.json())
        .then(data =>{
            setEntries(data)
        })
        .catch(err => console.log(err))
        }
    },[isEditing, entries, setIsEditing])

    const handleUpdateEntries = (newData) => {
        return setEntries([
            newData,
            ...entries
        ])
    }

    console.log('ENTRIES: ', entries)

    return (
        <div className="row">
            <div className="col-md-3">
               
            </div>
            <div className="col-md-6">
                <AddEntry updateEntries={(newData) => handleUpdateEntries(newData)} />
            </div>
            <div className="col-md-3">
                
            </div>
            <div className="col-md-3">
               
            </div>
            <div className="col-md-6">
            </div>
            <div className="col-md-3">  
            </div>
            <div className="col-md-3">
               
            </div>
            <div className="col-md-6">
                <ListEntry
                    entries={entries}
                    setEntries={setEntries}
                    setIsEditing={setIsEditing}
                    setEntryEditFields={setEntryEditFields}
                />
            </div>
            <div className="col-md-3">
                
            </div>
            <EditEntryModal visible={isEditing} setIsEditing={setIsEditing} editFields={editFields} setEntries={setEntries} entries={entries} />
        </div>  
        
    )
}

export default Entry;


