import React, {useState} from 'react'
import { Modal } from 'react-bootstrap'

import AddCategory from './../components/AddCategory'
import EditCategory from './../components/EditCategory'
import ListCategory from './../components/ListCategory'

const EditCategoryModal = ({visible, categories, setIsEditing, editFields, setCategories}) => {
  
    const handleClose = (data, editSuccess) => {
        const formattedCategories = categories.filter(category => category._id !== editFields._id)

        setIsEditing(false)
        editSuccess && setCategories([data, ...formattedCategories,])
    }
  
    return (  
        <Modal show={visible} onHide={handleClose} animation={false}>
          <Modal.Header closeButton>
            <Modal.Title>Edit Entry</Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <EditCategory onClose={(data, editSuccess) => handleClose(data, editSuccess)} editFields={editFields} setCategories={setCategories} />
          </Modal.Body>
        </Modal>
    );
  }
function Category() {
    const [isEditing, setIsEditing] = useState(false)
    const [editFields, setCategoryEditFields] = useState(null)
    const [categories, setCategories] = useState([])

    const handleUpdateCategories = (newData) => {
        return setCategories([
            newData,
            ...categories
        ])
    }

    return (
    <div className="row">
        <div className="col-md-3">  
            </div>
                <div className="col-md-6">
                        <AddCategory updateCategories={(newData) => handleUpdateCategories(newData)} />
                </div>
            <div className="col-md-3">        
        </div>
        <div className="col-md-3">      
            </div>
                <div className="col-md-6">
                        <EditCategoryModal visible={isEditing} setIsEditing={setIsEditing} editFields={editFields} setCategories={setCategories} categories={categories} />
                </div>
            <div className="col-md-3">        
        </div>
        <div className="col-md-3">      
            </div>
                <div className="col-md-6">
                        <ListCategory
                            categories={categories}
                            setCategories={setCategories}
                            setCategoryEditFields={setCategoryEditFields}
                            setIsEditing={setIsEditing}
                        />
                </div>
            <div className="col-md-3">        
        </div>
    </div>
    )
}

export default Category;
