import React, { useState, useContext } from "react";
import { Form, Button } from 'react-bootstrap'
import { ApplicationContext } from '../contexts/ApplicationContext'



function EditCategory(props) {
    const {editFields, onClose} = props

    const[fields, setFieldValue ] = useState({
        name: editFields.name,
    })

    const  {categories} = useContext(ApplicationContext)

    const[isLoading,setIsLoading]= useState(false)
 
    const handleSubmit = e =>{
        e.preventDefault();
        setIsLoading(true);
        fetch(`http://localhost:8000/api/category/${editFields._id}`,{
            method: "PUT",
            body: JSON.stringify(fields),
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            }

        })
        .then(res=>{
            setIsLoading(false);
            return res.json()
        })
        .then(data=>{
            onClose(data, true)
        })
        .catch(err => {
            return console.log(err)
        })

    }
    const handleChange = e =>{

        setFieldValue({
            ...fields,
            [e.target.id] : e.target.value
        })
    }
return (
            <Form onSubmit={handleSubmit}>

                <Form.Group controlId ="name">
                    <Form.Label>Add Category</Form.Label>
                    <Form.Control
                        type="text"
                        value={fields.name}
                        onChange={handleChange}
                    />
                </Form.Group>
                
                {     isLoading ?
                        <Button type="submit" disabled>Submit</Button>
                    :   <Button type="submit" >Submit</Button>
                }
            </Form>
      );
}

export default EditCategory;