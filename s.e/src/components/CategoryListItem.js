import React from 'react'
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import { ApplicationContext } from './../contexts/ApplicationContext';
import { useContext } from 'react'

function CategoryListItem({category, setLastDeletedEntry, setIsEditing, setCategoryEditFields}){

    const handleClick = e =>{
     e.preventDefault();
    fetch(`http://localhost:8000/api/category/${category._id}`,{
        method: 'DELETE',
        headers:{
           'Authorization' : `Bearer ${localStorage.getItem('token')}` 
        }
    })
    .then(res=> res.json())
    .then(data=>{
        setLastDeletedEntry(data)
    })
    .catch(err=> console.log(err))
    }   

    const handleOnPressEdit = e => {
        e.preventDefault();
        setIsEditing(true)
        setCategoryEditFields(category)
    }
    if (!category) {
        return <></>
    }

    return (
        <Card>
            <Card.Body>
                <Card.Title>{category.name}</Card.Title>
                <Button
                    onClick={handleOnPressEdit}
                    className="my-1"
                    variant="primary"
                    block
                    >
                    Edit
                </Button>

                <Button
                    className= "my-1"
                    variant = "danger"
                    onClick={handleClick}
                    block>
                    DELETE
                </Button>
            </Card.Body>
        </Card>
    )
}

export default CategoryListItem