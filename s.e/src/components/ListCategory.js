import React from 'react'
import { Container, Row, Col } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import CategoryListItem from './CategoryListItem';

export default function ListCategory(props) {

    const {categories, setCategories, setIsEditing, setCategoryEditFields} = props

    const [lastDeletedCategory, setlastDeletedCategory] = useState({})

    useEffect(()=>{
        
        let access = localStorage.getItem('token');
        fetch('http://localhost:8000/api/category/',{
          headers:{
              Authorization: `Bearer ${access}`
          }  
        })
        .then(res => res.json())
        .then(data =>{
            setCategories(data)
        })
        .catch(err => console.log(err))
    },[setlastDeletedCategory])

    const categoryDisplay = categories.map(category =>{

        return(
            <Col xs={12} sm={6} md={4} lg={3} key={categories._id}>
                <CategoryListItem
                    category={category}
                    viewBtn={true}
                    setlastDeletedCategory={setlastDeletedCategory}
                    setIsEditing={setIsEditing}  
                    setCategoryEditFields={setCategoryEditFields} 
                />
            </Col>
        )
    })
    return (
        <Container className="my-5">
            <Row>
                {categoryDisplay}
            </Row>
        </Container>
    )
}
