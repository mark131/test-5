import React, { useState, useContext } from "react";
import { Form, Button } from 'react-bootstrap'
import { ApplicationContext } from '../contexts/ApplicationContext'

function EditEntry(props) {
    const {editFields, onClose} = props
    const  {categories} = useContext(ApplicationContext)

    const[fields, setFieldValue ] = useState({
        entryName: editFields.entryName,
        amount: editFields.amount,
        categoryId: editFields.categoryId
    })

    const[isLoading, setIsLoading]= useState(false)


    let category = []

    if (!isLoading){
        category = categories.map((category,index)=>{
            return  <option key={index } value={category._id}>{category.name}</option>
        })
    }

    const handleSubmit = e =>{
        e.preventDefault();
        setIsLoading(true);
        fetch(`http://localhost:8000/api/Entry/${editFields._id}`,{
            method: "PUT",
            body: JSON.stringify(fields),
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            onClose(data)  
        })
        .catch(err => {
            return console.log(err)
        })
    }
    const handleChange = e =>{
        setFieldValue({
            ...fields,
            [e.target.id] : e.target.value
        })
    }

    return (
        <Form onSubmit={handleSubmit}>
            <Form.Group controlId ="entryName">
                <Form.Label>Add Budget Entry</Form.Label>
                <Form.Control
                    type="text"
                    value={fields.entryName}
                    onChange={handleChange}
                />
            </Form.Group>

            <Form.Group controlId ="amount">
                <Form.Label>Amount</Form.Label>
                <Form.Control

                    type="Number"
                    value={fields.amount}
                    onChange={handleChange}
                />
            </Form.Group>

            <Form.Group controlId="categoryId">
                <Form.Control as="select" onChange={handleChange} value={fields.categoryId} >
                    <option disabled>category</option>{category}
                </Form.Control>

                </Form.Group>


            {     isLoading ?
                    <Button type="submit" disabled>Submit</Button>
                :   <Button type="submit" >Submit</Button>
            }
        </Form>
      );
}

export default EditEntry;