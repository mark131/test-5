import React from 'react'
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import { ApplicationContext } from './../contexts/ApplicationContext';
import { useContext } from 'react'


const getCategory = (categories, key) => {
    // console.log('categories: ',categories)
    // console.log('key:', key)
    if(categories && categories > 0) {
        return categories.filter(a => a._id === key)[0].name
    }
    return ''
}
function EntryListItem({entry,setLastDeletedEntry, setIsEditing, setEntryEditFields}){

    const handleClick = e =>{
     e.preventDefault();
    fetch(`http://localhost:8000/api/Entry/${entry._id}`,{
        method: 'DELETE',
        headers:{
           'Authorization' : `Bearer ${localStorage.getItem('token')}` 
        }
    })
    .then(res=> res.json())
    .then(data=>{
        setLastDeletedEntry(data)
    })
    .catch(err=> console.log(err))
    }

    const handleOnPressEdit = e => {
        e.preventDefault();
        setIsEditing(true)
        setEntryEditFields(entry)
    }

    if (!entry) {
        return <></>
    }

    return (
        <Card>
            <Card.Body>
                <Card.Title>Entry Name:{entry.entryName}</Card.Title>
                <Card.Title>Amount:{entry.amount}</Card.Title>
                {/* not showing category name */}
                <Card.Title>{entry.categoryName}</Card.Title> 
                <Button onClick={handleOnPressEdit} className="my-1" variant="primary" block>Edit</Button>

                <Button
                className= "my-1"
                variant = "danger"
                onClick={handleClick}
                block>DELETE</Button>
            </Card.Body>
        </Card>
    )
}

export default EntryListItem
