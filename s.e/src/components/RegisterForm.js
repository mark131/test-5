
import { Form, Button } from 'react-bootstrap'
import { useState, useContext } from 'react'


    
export default function RegisterForm({setIsRedirect}) {

    const [registration,setRegistrations] = useState({
        name: "",
        email: "",
        password: ""
    })
    

    const handleSubmit = e =>{
        e.preventDefault()
        fetch('http://localhost:8000/api/users',{
          
            method: "POST",
			body: JSON.stringify(registration),
			headers: {
				"Content-Type" : "application/json"
			}
            
        })
        .then( res => {
            console.log(res)
            if(res.status === 200){
                setIsRedirect(true)
                alert("Registration Successful")
                return res.json()
               
            }else{
                alert("Registration failed")
                throw new Error("Failed to Register")
            }
            
        })
        .catch(err => console.log( err ))
    }
    
    const handleChange = e =>{
        setRegistrations({
            ...registration,
            [e.target.id] : e.target.value
        })
        
    }
   
    return(
        <Form onSubmit={handleSubmit}>

            <Form.Group controlId="name">
                <Form.Label>Name:</Form.Label>
                <Form.Control 
                    type="name"
                    onChange={handleChange}
             
                />
            </Form.Group>

            <Form.Group controlId="email">
                <Form.Label>Email:</Form.Label>
                <Form.Control 
                    type="email"
                    onChange={handleChange}
             
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label placeholder="password must be 8 char">Password:</Form.Label>
                <Form.Control 
                    type="password"
                    onChange={handleChange}
                />
            </Form.Group>

         <Button type="submit" >Register</Button>
            
        </Form>
    ) 
}