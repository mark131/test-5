import {Navbar,Nav} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import  {useContext} from 'react'
import {ApplicationContext} from './../contexts/ApplicationContext'



export default function MainNav() {

	const {isLoading, setIsLoading} = useContext(ApplicationContext)
	const { user,setUser } = useContext(ApplicationContext)
	
	const handleClick=()=>{
		
		setUser({
			userId : "",
			isAdmin: false,
			email : "",
			name: ""
		})
		alert('Logout Successful')
		localStorage.clear()
	}


	const navLinks = !user.userId ?
		<>
		<Nav.Link as={Link} to="/login">Login</Nav.Link>
		<Nav.Link as={Link} to="/register">Register</Nav.Link>
		</>:
		<>
		<Nav.Link as={Link} to="/category">Add/Edit Categories</Nav.Link>
        <Nav.Link as={Link} to="/entry">Add/Edit Entries</Nav.Link>
        <Nav.Link onClick={handleClick} to="/">Logout</Nav.Link> 
		<Nav className="ml-auto"></Nav>
		</> 
	return (
		
		<div>
		<Navbar bg="light" expand="md">
		<Navbar.Brand as={Link} to="/">Expense Tracker</Navbar.Brand>
		{navLinks}
		</Navbar>
		</div>
	)
}

