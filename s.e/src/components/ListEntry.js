import React, {useContext} from 'react'
import { Container, Row, Col } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import EntryListItem from './EntryListItem';
import { ApplicationContext } from './../contexts/ApplicationContext';

const getCategory = (categories, key) => {
    if(categories && categories > 0) {
        return categories.filter(a => a._id === key)[0].name
    }
    return ''
}


export default function ListEntry(props) {

    const {entries, setEntries, setIsEditing, setEntryEditFields} = props

    const [lastDeletedEntry,setLastDeletedEntry] = useState({})
    const [isLoading, setIsLoading] = useState(false)
    const  {categories} = useContext(ApplicationContext)

    useEffect(()=>{
        
        let access = localStorage.getItem('token');
        fetch('http://localhost:8000/api/Entry',{
          headers:{
              Authorization: `Bearer ${access}`
          }  
        })
        .then(res => res.json())
        .then(data =>{
            setEntries(data.map(a => ({
                ...a,
                categoryName: getCategory(categories, a.categoryId)
            })))
        })
        .catch(err => console.log(err))
    },[setLastDeletedEntry])


    const entryDisplay = entries.length > 0 ? entries.map(entry =>{

        return(
            <Col xs={12} sm={6} md={4} lg={3} key={entries._id}>
                <EntryListItem
                    entry={entry}
                    viewBtn={true}
                    setLastDeletedEntry={setLastDeletedEntry}
                    setIsEditing={setIsEditing}  
                    setEntryEditFields={setEntryEditFields}              />
            </Col>
        )
    }) : []

    return (
        <Container className="my-5">
            <Row>
                {!isLoading && entryDisplay}
                {isLoading && 'Entries are Loading...'} 
            </Row>
        </Container>
    )
}


