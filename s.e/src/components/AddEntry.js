import React, { useState, useContext } from "react";
import { Form, Button } from 'react-bootstrap'
import { ApplicationContext } from '../contexts/ApplicationContext'



function AddEntry(props) {

    const  {categories} = useContext(ApplicationContext)
    const {updateEntries} = props


    const[entries, setEntries] = useState({
        entryName: "",
        amount: "",
        categoryId: ""
    })

    const[isLoading,setIsLoading]= useState(false)


    let category = []

    if (!isLoading){
        category = categories.map((category,index)=>{
            return  <option key={index } value={category._id}>{category.name}</option>
        })

        if (categories && categories.length > 0 && entries.categoryId === '') {
            return setEntries({
                ...entries,
                categoryId: categories[0]._id,
            })
        }
    }

    const handleSubmit = e =>{
        e.preventDefault();
        setIsLoading(true);
        fetch('http://localhost:8000/api/Entry',{
            method: "POST",
            body: JSON.stringify(entries),
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            }

        })
        .then(res=>{
            setIsLoading(false);
            return res.json()
        })
        .then(data=>{
            setEntries({
                entryName: "",
                amount: "",
                categoryId: ""
            })
            updateEntries(data)
        })
        .catch(err => {
            return console.log(err)
        })

    }
    const handleChange = e =>{

        setEntries({
            ...entries,
            [e.target.id] : e.target.value
        })
    }
return (
            <Form onSubmit={handleSubmit}>

                <Form.Group controlId ="entryName">
                    <Form.Label>Add Budget Entry</Form.Label>
                    <Form.Control
                        type="text"
                        value={entries.entryName}
                        onChange={handleChange}
                    />
                </Form.Group>

                <Form.Group controlId ="amount">
                    <Form.Label>Amount</Form.Label>
                    <Form.Control

                        type="Number"
                        value={entries.amount}
                        onChange={handleChange}
                    />
                </Form.Group>


                <Form.Group controlId="categoryId">
                   <Form.Control as="select" onChange={handleChange} value={category.length > 0 && category[0]._id} >
                        <option disabled>category</option>{category}
                    </Form.Control>

                 </Form.Group>


                {     isLoading ?
                        <Button type="submit" disabled>Submit</Button>
                    :   <Button type="submit" >Submit</Button>
                }
            </Form>
      );
}

export default AddEntry;