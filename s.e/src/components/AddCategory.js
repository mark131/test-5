import React, { useState, useContext } from "react";
import { Form, Button } from 'react-bootstrap'
import { ApplicationContext } from '../contexts/ApplicationContext'



function AddCategory(props) {
    const {updateCategories} = props

    const  {categories} = useContext(ApplicationContext)

    const[category, setCategory] = useState({
        name:""
    })

    const[isLoading,setIsLoading]= useState(false)
 
    const handleSubmit = e =>{
        e.preventDefault();
        setIsLoading(true);
        fetch('http://localhost:8000/api/category',{
            method: "POST",
            body: JSON.stringify(category),
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res=>{
            setIsLoading(false);
            return res.json()
        })
        .then(data=>{
            setCategory({
                name:""
            })
            updateCategories(data)
        })
        .catch(err => {
            return console.log(err)
        })

    }
    const handleChange = e =>{

        setCategory({
            ...category,
            [e.target.id] : e.target.value
        })
    }
return (
            <Form onSubmit={handleSubmit}>

                <Form.Group controlId ="name">
                    <Form.Label>Add Category</Form.Label>
                    <Form.Control
                        type="text"
                        value={category.name}
                        onChange={handleChange}
                    />
                </Form.Group>
                
                {     isLoading ?
                        <Button type="submit" disabled>Submit</Button>
                    :   <Button type="submit" >Submit</Button>
                }
            </Form>
      );
}

export default AddCategory;