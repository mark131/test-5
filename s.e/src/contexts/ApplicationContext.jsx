import {createContext,useState,useEffect} from 'react'

export const ApplicationContext = createContext()


export default function ApplicationProvider(props) {

	const [ isLoading, setIsLoading ] = useState(false)

	const [user, setUser] = useState([])

	const [ categories, setCategories] = useState([])

	const	[ entries, setEntries ] = useState([])

	
	
	useEffect(()=>{
		setIsLoading(true)
		fetch('http://localhost:8000/api/category/all',{
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>{
			setIsLoading(false)
			return res.json()
		})
		.then(data=>{
			setCategories(data)
			console.log(data)
		})
		.catch(err=> console.log.err)

	},[])

	// useEffect(()=>{
	// 	setIsLoading(true)
	// 	fetch('http://localhost:8000/api/Entry',{
	// 		headers: {
	// 			'Authorization': `Bearer ${localStorage.getItem('token')}`
	// 		}
	// 	})
	// 	.then(res=>{
	// 		setIsLoading(false)
	// 		return res.json()
	// 	})
	// 	.then(entry=>{
	// 		setCategories(entry)
	// 		console.log(entry)
	// 	})
	// 	.catch(err=> console.log.err)

	// },[])
	
	

	
	
	useEffect( ()=>{
		if(!localStorage.token){
		}else{
		fetch('http://localhost:8000/api/users',{
			headers: {
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		}) 
		
		.then( res => res.json())
		.then( data => {
			
			let { firstName, lastName, isAdmin, email } = data
			setUser({
				userId: data._id,
				firstName,
				lastName,
				email,
				entries
			})
		})
		.catch( err =>console.log( err ))
	}	
	},[])


    return(
		<ApplicationContext.Provider
			value={{
				setUser,
				user,
				categories,
				setCategories	
			}}
		>
			{props.children}
		</ApplicationContext.Provider>
	)

}

