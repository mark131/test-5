import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Home from './pages/Home'
import Login from './pages/Login'
import Register from  './pages/Register'
import Entry  from './pages/Entry'
import Category from './pages/Category'
import MainNav from  './components/MainNav'
import { 
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'
import ApplicationProvider from'./contexts/ApplicationContext'

function App() {
  return (
    <div className="App">
        <ApplicationProvider>
          <Router>
                <MainNav/>
              <Switch>
                <Route exact path="/">
                  <Home />
                </Route>
                <Route path="/category">
                  <Category />
                </Route>
                <Route  path="/entry">
                  <Entry />
                </Route>
                <Route path="/login">
                    <Login />
                </Route>
                <Route path="/register">
                  <Register />
                </Route>
              </Switch>
          </Router>
          </ApplicationProvider>  
    </div>
  );
}

export default App;
<style>
@import "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css";
</style>
