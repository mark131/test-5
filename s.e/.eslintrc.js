module.exports = {
    root: true,
    extends: ['@react-native-community', 'plugin:sonarjs/recommended'],
    rules: {
      'prettier/prettier': ['error', {endOfLine: 'auto'}],
      'no-unused-vars': 'error',
    },
    plugins: ['sonarjs'],
  };
  